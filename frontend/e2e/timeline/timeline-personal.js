(function () {
  'use strict';

  var PersonalTimeline = require('./timeline-personal.page.js');
  var login = require('../login.page.js');
  var path = require('path');

  describe('personal timeline', function () {
    var personalTimeline;

    beforeEach(function () {
      personalTimeline = new PersonalTimeline();

      login.loginDefaultUser();
    });

    it('should load items', function () {
      expect(personalTimeline.stream.isPresent()).toBe(true);
      expect(personalTimeline.items.count()).toBeGreaterThan(0);
    });

    // eslint-disable-next-line jasmine/no-disabled-tests
    xit('create a timeline post with attachment', function () {
      var message = 'Hello World ' + Math.floor(Math.random() * 1000000);
      personalTimeline.form.messageField.sendKeys(message);

      var fileToUpload = 'upload.txt';
      var absolutePath = path.resolve(__dirname, fileToUpload); //eslint-disable-line
      console.log('Uploading file from: ', absolutePath); //eslint-disable-line

      personalTimeline.form.attachmentTrigger.click();
      $('input[type=file]').sendKeys(absolutePath);

      personalTimeline.form.submitBtn.click();

      browser.sleep(300); // wait a little bit
      expect(element(by.cssContainingText('.timeline-item-message', message)).isPresent()).toBe(true);
      expect(element(by.cssContainingText('.timeline-item-attachment-name', fileToUpload)).isPresent()).toBe(true);
    }).pend('test cannot run in docker env as upload file is not available');
  });

})();
