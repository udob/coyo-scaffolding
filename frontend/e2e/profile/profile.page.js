(function () {
  'use strict';

  function Profile() {
    var api = this;

    var navbarProfile = $('.profile-nav');
    var groups = {
      contact: $$('.profile-group').get(0),
      work: $$('.profile-group').get(2)
    };

    /*Profile activity*/
    api.wall = {
      addProfilePic: element.all(by.css('[ng-click="clickFn()"]')).get(2),
      addCoverPic: $('.btn-dark'),
      fileUpload: element.all(by.css('input[type=file]')).last(),
      submitFileUpload: $('[ng-click="vm.upload(vm.croppedImage)"]'),
      postOnWallTextarea: element.all(by.css('textarea[ng-model="$ctrl.newItemModel.data.message"]')),
      postOnWallSubmit: element.all(by.css('[form-ctrl="$ctrl.form"]')).first()
    };

    /*Profile info*/
    api.contact = {
      phone: groups.contact.element(by.css('#editable-contact-phone .form-control-static')),
      mobile: groups.contact.element(by.css('#editable-contact-mobile .form-control-static')),
      xing: groups.contact.element(by.css('#editable-contact-xing .form-control-static a')),
      linkedin: groups.contact.element(by.css('#editable-contact-linkedin .form-control-static a')),
      twitter: groups.contact.element(by.css('#editable-contact-twitter .form-control-static a')),
      facebook: groups.contact.element(by.css('#editable-contact-facebook .form-control-static a')),
      phoneInput: groups.contact.$('#input-editable-contact-phone'),
      mobileInput: groups.contact.$('#input-editable-contact-mobile'),
      xingInput: groups.contact.$('#input-editable-contact-xing'),
      linkedinInput: groups.contact.$('#input-editable-contact-linkedin'),
      twitterInput: groups.contact.$('#input-editable-contact-twitter'),
      facebookInput: groups.contact.$('#input-editable-contact-facebook'),
      editButton: groups.contact.$('.btn-edit'),
      submitButton: groups.contact.$('button[type="submit"]')
    };

    api.work = {
      form: groups.work,
      jobTitle: groups.work.element(by.css('#editable-work-jobTitle .form-control-static')),
      company: groups.work.element(by.css('#editable-work-company .form-control-static')),
      department: groups.work.element(by.css('#editable-work-department .form-control-static')),
      location: groups.work.element(by.css('#editable-work-location .form-control-static')),
      jobTitleInput: groups.work.element(by.css('#input-editable-work-jobTitle')),
      companyInput: groups.work.element(by.css('#input-editable-work-company')),
      departmentInput: groups.work.element(by.css('#input-editable-work-department')),
      locationInput: groups.work.element(by.css('#input-editable-work-location')),
      editButton: groups.work.$('.btn-edit'),
      submitButton: groups.work.$('button[type="submit"]')
    };

    api.info = navbarProfile.$('li[ui-sref="main.profile.info"]');
  }

  module.exports = Profile;

})();

