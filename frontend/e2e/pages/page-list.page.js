(function () {
  'use strict';

  function PageList() {
    var api = this;

    api.newButton = $('.actions-vertical a[ui-sref="main.page.create"]');
    api.search = function (pageName) {
      element(by.model('$ctrl.searchTerm')).sendKeys(pageName);
      return element.all(by.css('.page-card'));
    };
    api.filterAll = $('.panel-filterbox').element(by.css('li[text-key="MODULE.PAGES.FILTER.ALL"] a'));
  }

  module.exports = PageList;

})();
