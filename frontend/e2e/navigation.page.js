(function () {
  'use strict';

  function Navigation() {
    var api = this;

    var navbar = $('.main-navigation .msm-navbar');

    api.home = navbar.$('a[ui-sref="main.landing-page"]');
    api.pages = navbar.$('a[ui-sref="main.page"]');
    api.searchIcon = navbar.$('.nav-search .search-icon');
    api.notification = navbar.$('.notifications-dialog-parent .zmdi-notifications');
    api.colleagues = navbar.$('a[ui-sref="main.colleagues"]');
    api.profile = navbar.$('a[ui-sref="main.profile({userId: $ctrl.user.slug})"]');
    api.profileMenu = {
      open: function () {
        api.profileMenu.toggle.click();
      },
      toggle: navbar.$('.nav-item-profile .zmdi-caret-down'),
      admin: navbar.$('a[ui-sref="admin"]'),
      editView: navbar.$('[translate="NAVIGATION.EDIT.VIEW"]'),
      account: navbar.$('a[ui-sref="main.account"]'),
      notifications: navbar.$('a[ui-sref="main.account-notifications"]'),
      fileLibrary: navbar.$('a[coyo-file-library-trigger]'),
      logout: navbar.$('a[ng-click="$ctrl.logout()"]')
    };
    api.admin = {
      exit: $('a[ui-sref="main"] span[translate="ADMIN.MENU.EXIT"]'),
      userDirectories: $('a[ui-sref="admin.user-directories.list"]'),
      authProviders: $('a[ui-sref="admin.authentication-providers.list"]')
    };
    api.logout = function () {
      api.profileMenu.open();
      api.profileMenu.logout.click();
    };
    api.editView = function () {
      api.profileMenu.open();
      api.profileMenu.editView.click();
    };
    api.viewEditOptions = {
      saveButton: $('.view-edit-options-bar a[ng-click="$ctrl.save()"]'),
      cancelButton: $('.view-edit-options-bar a[ng-click="$ctrl.cancel()"]')
    };
  }

  module.exports = Navigation;

})();
