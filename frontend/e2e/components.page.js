(function () {
  'use strict';

  module.exports = {
    modals: {
      confirm: {
        // :not(.disabled) excludes all elements with class="btn-primary disabled ..."
        confirmButton: element(by.css('.modal-footer .btn-primary:not(.disabled)')),
        cancelButton: element(by.css('.modal-footer .btn-default'))
      }
    }
  };

})();
