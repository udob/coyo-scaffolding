(function () {
  'use strict';

  var login = require('./../../login.page.js');
  var list = require('./admin.user-list.page.js');
  var details = require('./admin.user-details.page.js');
  var components = require('./../../components.page.js');

  describe('user administration', function () {

    beforeEach(function () {
      login.loginDefaultUser();
      list.get();
    });

    it('should filter list', function () {
      // apply filter
      list.nameFilter.clear();
      list.nameFilter.sendKeys('nancy');

      expect(list.userTotal.getText()).toBe('1 User');
      expect(list.table.rows.count()).toBe(1);

      var row = list.table.rows.get(0);
      expect(row.name.getText()).toContain('Nancy Fork');
      expect(row.email.getText()).toBe('nancy.fork@coyo4.com');
      expect(row.status.getText()).toBe('ACTIVE');

      // reset filter
      list.nameFilter.clear();
      expect(list.table.rows.count()).toBeGreaterThan(1);
    });

    it('should sort list', function () {

      // sort by email
      verifySorting(list.table.headers.email, list.table.rows.emails);

      // sort by name
      verifySorting(list.table.headers.name, list.table.rows.names);

      function verifySorting(header, data) {
        // asc
        header.click();
        data().then(function (values) {
          expect(values).toEqual(values.slice().sort());
        });

        // desc
        header.click();
        data().then(function (values) {
          expect(values).toEqual(values.slice().sort().reverse());
        });
      }
    });

    it('should delete and recover user', function () {
      // delete a user
      list.statusFilter.showAll();
      list.nameFilter.clear();
      list.nameFilter.sendKeys('nancy');
      expect(list.table.rows.count()).toBe(1);

      openOptions().deleteOption.click();
      components.modals.confirm.confirmButton.click();

      // verify that the user was deleted
      list.statusFilter.showActive();
      expect(list.table.rows.count()).toBe(0);
      list.statusFilter.showDeleted();
      expect(list.table.rows.count()).toBe(1);
      expect(list.table.rows.get(0).name.getText()).toContain('Nancy Fork');

      // recover the deleted user
      openOptions().recoverOption.click();
      components.modals.confirm.confirmButton.click();

      // verify that the user was recovered
      list.statusFilter.showInactive();
      expect(list.table.rows.count()).toBe(1);
      expect(list.table.rows.get(0).name.getText()).toContain('Nancy Fork');

      openOptions().activateOption.click();
    });

    it('should de- and reactivate user', function () {
      // deactivate a user
      list.statusFilter.showAll();
      list.nameFilter.clear();
      list.nameFilter.sendKeys('nancy');

      expect(list.table.rows.count()).toBe(1);

      openOptions().deactivateOption.click();

      // verify that the user was deactivated
      list.statusFilter.showActive();
      expect(list.table.rows.count()).toBe(0);
      list.statusFilter.showInactive();
      expect(list.table.rows.count()).toBe(1);
      expect(list.table.rows.get(0).name.getText()).toContain('Nancy Fork');

      // reactivate the user
      openOptions().activateOption.click();

      // verify that the user was reactivated
      list.statusFilter.showActive();
      expect(list.table.rows.count()).toBe(1);
      expect(list.table.rows.get(0).name.getText()).toContain('Nancy Fork');
    });

    it('should create and update user', function () {
      var suffix = Math.floor(Math.random() * 1000000); // for unique test data
      var email = 'testfirst.testlast' + suffix + '@mindsmash.com';
      var firstname = 'testfirst';
      var lastname = 'testlast' + suffix;
      var password = 'Secret123';

      // create user
      list.createButton.click();
      expect(details.isSaveButtonDisabled()).toBe(true);
      details.email.sendKeys('not an email');
      details.firstname.sendKeys(firstname);
      details.lastname.sendKeys(lastname);
      details.roles.openDropdown();
      details.roles.selectOption('User');
      details.roles.search('A');
      details.roles.selectOption('Admin');
      details.active.click();
      details.password.sendKeys('invalid password');
      expect(details.isSaveButtonDisabled()).toBe(true);

      details.password.clear();
      details.password.sendKeys(password);
      expect(details.isSaveButtonDisabled()).toBe(true);
      details.email.clear();
      details.email.sendKeys(email);
      expect(details.isSaveButtonDisabled()).toBe(false);
      details.saveButton.click();

      // verify that the user was created
      list.nameFilter.clear();
      list.nameFilter.sendKeys(lastname);
      expect(list.table.rows.count()).toBe(1);
      openOptions().editOption.click();

      expect(details.email.getAttribute('value')).toBe(email);
      expect(details.lastname.getAttribute('value')).toBe(lastname);
      expect(details.firstname.getAttribute('value')).toBe(firstname);
      expect(details.roles.selectedOptions()).toContain('User');
      expect(details.roles.selectedOptions()).toContain('Admin');
      expect(details.isActive()).toBe(true);

      // update user
      details.email.sendKeys('-updated');
      details.lastname.sendKeys('-updated');
      details.firstname.sendKeys('-updated');
      details.roles.removeOption('Admin');
      details.active.click();
      details.saveButton.click();

      // verify the updated user data
      list.statusFilter.showActive();
      list.nameFilter.clear();
      list.nameFilter.sendKeys(lastname + '-updated');
      expect(list.table.rows.count()).toBe(0);
      list.statusFilter.showInactive();
      expect(list.table.rows.count()).toBe(1);
      openOptions().editOption.click();

      expect(details.email.getAttribute('value')).toBe(email + '-updated');
      expect(details.lastname.getAttribute('value')).toBe(lastname + '-updated');
      expect(details.firstname.getAttribute('value')).toBe(firstname + '-updated');
      expect(details.roles.selectedOptions()).not.toContain('Admin');
      expect(details.roles.selectedOptions()).toContain('User');
      expect(details.isActive()).toBe(false);
    });

    function openOptions() {
      var options = list.table.rows.get(0).options();
      options.open();
      return options;
    }
  });

})();
