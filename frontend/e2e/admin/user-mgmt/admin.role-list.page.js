(function () {
  'use strict';

  var repeater = 'row in vm.page.content track by row.id';

  module.exports = {
    get: function () {
      browser.get('/admin/user-management/roles');
      require('../../testhelper.js').disableAnimations();
    },
    nameFilter: element(by.model('$ctrl.searchTerm')),
    rolesTotal: $('counter span.hidden-xs'),
    createButton: $('.fb-actions-inline a[ui-sref="admin.user-management.roles.create"]'),
    table: {
      headers: {
        name: element(by.css('th[property="displayName"]'))
      },
      rows: {
        get: function (index) {
          var row = $$('tr[ng-repeat="' + repeater + '"]').get(index);
          return {
            row: row,
            name: row.$$('td').get(0),
            groups: row.$$('td').get(1),
            users: row.$$('td').get(2),
            options: function () {
              var el = row.$$('td').get(3);
              return {
                open: function () {
                  browser.actions().mouseMove(el).perform();
                  el.$('.dropdown-toggle').click();
                },
                deleteOption: el.$('span[translate="ADMIN.USER_MGMT.ROLES.OPTIONS.DELETE.MENU"]'),
                editOption: el.$('span[translate="ADMIN.USER_MGMT.ROLES.OPTIONS.EDIT.MENU"]')
              };
            }
          };
        },
        names: function () {
          return $$('tr[ng-repeat="' + repeater + '"]').map(function (row) {
            return row.$$('td').get(0).getText();
          });
        },
        count: function () {
          return element.all(by.repeater('' + repeater + '')).count();
        }
      },
      pagination: {
        links: element.all(by.css('.pagination-page')),
        first: $('.pagination-first'),
        prev: $('.pagination-prev'),
        next: $('.pagination-next'),
        last: $('.pagination-last')
      }
    }
  };

})();
