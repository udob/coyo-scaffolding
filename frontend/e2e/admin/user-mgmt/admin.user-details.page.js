(function () {
  'use strict';

  var Select = require('../../select.page.js');

  function hasClass(el, className) {
    return el.getAttribute('class').then(function (value) {
      return value.split(' ').indexOf(className) >= 0;
    });
  }

  module.exports = {
    getById: function (id) {
      browser.get('/admin/user-management/users/edit/' + id);
    },
    email: element(by.model('$ctrl.user.email')),
    firstname: element(by.model('$ctrl.user.firstname')),
    lastname: element(by.model('$ctrl.user.lastname')),
    roles: new Select(element(by.model('$ctrl.roles')), true),
    active: element(by.model('$ctrl.user.active')),
    isActive: function () {
      return hasClass(this.active, 'checked');
    },
    password: element(by.model('$ctrl.user.password')),
    saveButton: element(by.cssContainingText('.btn.btn-primary', 'Save')),
    isSaveButtonDisabled: function () {
      return this.saveButton.getAttribute('disabled').then(function (disabled) {
        return disabled === 'true';
      });
    },
    cancelButton: element(by.css('.btn', 'Cancel'))
  };

})();
