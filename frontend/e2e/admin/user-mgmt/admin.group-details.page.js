(function () {
  'use strict';

  var Select = require('../../select.page.js');

  module.exports = {
    getById: function (id) {
      browser.get('/admin/user-management/groups/edit/' + id);
    },
    name: element(by.model('vm.group.displayName')),
    roles: new Select(element(by.model('vm.roles')), true),
    saveButton: element(by.cssContainingText('.btn.btn-primary', 'Save')),
    isSaveButtonDisabled: function () {
      return this.saveButton.getAttribute('disabled').then(function (disabled) {
        return disabled === 'true';
      });
    },
    cancelButton: element(by.cssContainingText('.btn.btn-default', 'Cancel'))
  };

})();
