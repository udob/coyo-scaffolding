(function () {
  'use strict';

  module.exports = {
    connection: {
      heading: $('.uib-tab span[translate="ADMIN.USER_DIRECTORIES.LDAP.TABS.HEADINGS.CONNECTION"]')
    },
    searchCriteriaTab: {
      heading: $('.uib-tab span[translate="ADMIN.USER_DIRECTORIES.LDAP.TABS.HEADINGS.SEARCH"]'),
      baseDn: element(by.model('$ctrl.ngModel.settings.baseDn'))
    },
    userTab: {
      heading: $('.uib-tab span[translate="ADMIN.USER_DIRECTORIES.LDAP.TABS.HEADINGS.USER"]')
    },
    syncTab: {
      heading: $('.uib-tab span[translate="ADMIN.USER_DIRECTORIES.LDAP.TABS.HEADINGS.SYNC"]')
    }
  };

})();
