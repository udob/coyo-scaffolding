(function () {
  'use strict';

  module.exports = {
    get: function () {
      browser.get('/admin/theme');
      require('../../testhelper.js').disableAnimations();
    },
    colors: {
      primary: $('#variable_color-primary'),
      mainBackground: $('#variable_color-background-main')
    },
    saveButton: $('.panel-footer .btn-primary')
  };

})();
