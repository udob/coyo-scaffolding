(function () {
  'use strict';

  var components = require('../components.page');

  function Account() {
    var api = this;

    var container = $('.account > .panel:first-child');

    api.displayName = container.element(by.binding('vm.data.account.displayName'));
    api.changeName = function (firstname, lastname) {
      container.$('[ng-click="vm.openChangeNameModal()"]').click();
      element(by.model('vm.firstName')).clear().sendKeys(firstname);
      element(by.model('vm.lastName')).clear().sendKeys(lastname);
      $('[label="MODULE.ACCOUNT.MODALS.CHANGE_NAME.SUBMIT"]').click();
    };
    api.memberSince = container.element(by.binding('vm.data.account.created'));
    api.directory = container.element(by.binding('vm.data.account.directory'));
    api.language = container.element(by.binding('vm.data.account.language'));
    api.changeLanguageByTyping = function (language) {
      container.$('[ng-click="vm.openLanguageModal()"').click();
      element(by.model('vm.language')).click();
      element(by.model('$select.search')).sendKeys(language);
      $('.ui-select-choices-row-inner').click();
      $('[label="MODULE.ACCOUNT.MODALS.SELECT_LANGUAGE.SUBMIT"]').click();
    };
    api.changeLanguageByChooseIndex = function (index) {
      container.$('[ng-click="vm.openLanguageModal()"').click();
      element(by.model('vm.language')).click();
      var languageList = $$('.ui-select-choices-row');
      languageList.get(index).click();
      $('[label="MODULE.ACCOUNT.MODALS.SELECT_LANGUAGE.SUBMIT"]').click();
    };
    api.timeZone = container.element(by.binding('vm.data.account.timezone'));
    api.changeTimeZone = function (timezone) {
      container.$('[ng-click="vm.openTimeZoneModal()"]').click();
      element(by.model('vm.timeZone')).click();
      element(by.model('$select.search')).sendKeys(timezone);
      $('.ui-select-choices-row-inner').click();
      $('[label="MODULE.ACCOUNT.MODALS.SELECT_TIMEZONE.SUBMIT"]').click();
    };
    api.email = container.element(by.binding('vm.data.account.email'));
    api.changeEmail = function (newEmail) {
      container.$('i[ng-click="vm.openChangeEmailAddressModal()"]').click();
      element(by.model('vm.newEmailAddress')).sendKeys(newEmail);
      components.modals.confirm.confirmButton.click();
    };
    api.password = {
      openModal: function () {
        container.$('i[ng-click="vm.openChangePasswordModal()"]').click();
      },
      oldPassword: element(by.model('vm.oldPassword')),
      newPassword: element(by.model('vm.newPassword')),
      confirmNewPassword: element(by.model('vm.confirmNewPassword'))
    };
  }

  module.exports = Account;

})();
