(function () {
  'use strict';

  var extend = require('util')._extend;
  var Select = require('../../select.page');

  function SuggestPageWidget(widget) {
    var api = extend(this, widget);

    api.settings = {
      pageApp: new Select($('coyo-select-pages')),
      pageSelector: $('.ui-select-search')
    };

    api.renderedWidget = {
      suggestedPage: $('[ui-sref="main.page.show({ idOrSlug: page.id })"]')
    };
  }
  module.exports = SuggestPageWidget;
})();
