(function () {
  'use strict';

  var extend = require('util')._extend;
  var Checkbox = require('../../checkbox.page');

  function WelcomeWidget(widget) {
    var api = extend(this, widget);

    api.settings = {
      welcomeText: element(by.model('model.settings.welcomeText')),
      showCover: new Checkbox(element(by.model('model.settings._showCover')))
    };

    api.renderedWidget = {
      text: api.container.$('.user-name')
    };
  }

  module.exports = WelcomeWidget;

})();
