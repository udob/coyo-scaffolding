(function () {
  'use strict';

  var login = require('../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var WelcomeWidget = require('./welcome-widget.page');
  var testhelper = require('../../testhelper');

  describe('welcome widget', function () {
    var widgetSlot, widgetChooser, widget, navigation;

    beforeEach(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = new WelcomeWidget(widgetSlot.getWidget(0));

    });

    it('should be created', function () {
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Welcome');

      var settings = widget.settings;
      expect(settings.welcomeText.getAttribute('value')).toBe('Welcome!');
      expect(settings.showCover.isChecked()).toBe(false);

      settings.welcomeText.clear();
      settings.welcomeText.sendKeys('Welcome to Coyo!');
      settings.showCover.click();
      expect(settings.showCover.isChecked()).toBe(true);

      widgetChooser.saveButton.click();
      navigation.viewEditOptions.saveButton.click();
      expect(widget.renderedWidget.text.getText()).toBe('Welcome to Coyo!\nIan Bold');

    });
  });
})();
